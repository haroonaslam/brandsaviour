  export default {
  
   props:[
   'sidebars'
   ],
    components: {
      MainLayout,
      Sketch,
      ModalFile,
      SubScribe,
      uploadFile
    },
        data: function(){
        return {
        fileid:null,
        listgridicons:'',
        listicon:'dist/list_view.svg',
        listicondis:'dist/list_view_disabled.svg',        
        gridicon:'dist/grid_view_active.svg',
        gridicondis:'dist/grid_view.svg',

        // pagination
        total: 25,
        current: 1,
        perPage: 5,
        order: '',
        size: '',
        isSimple: false,
        // pagination

        // Local Storage Varaible
        username: localStorage.getItem("username"),
        token:localStorage.getItem("token"),
        // Local Storage Varaible

    name: '',
    dimension: '',
    width: '',
    height:'',
    start:0,
    end:3,    
    start1:0,
    end1:3,
    files: [],
    selected:[],
    filesarr:[],
    dataUrl:'',
    src: '',
    size:'',
    dlsrc:'',
    type:'',
    typeshow:'',
    contentType:'',
    icons:'',
    fileid:null,
        brand:'NEST I/O',
        colorsdet:[{
          id:0,
          colorid:'one',
          colorname:'Pomegranate',
          colorhex:'#EF4038',
          colorrgb:'(239, 64, 56)',
          colorhsl:'(3, 85%, 58%)',
          colorcmyk:'(0%, 73%, 77%, 6%)'
        },
        {
          id:1,
          colorid:'two',
          colorname:'Lochmara',
          colorhex:'#0480BF',
          colorrgb:'(4, 128, 191)',
          colorhsl:'(200, 96%, 38%)',
          colorcmyk:'(98%, 33%, 0%, 25%)'
        }, 
        {
          id:2,
          colorid:'three',
          colorname:'Alizarin Crimson',
          colorhex:'E92129',
          colorrgb:'(233, 33, 41)',
          colorhsl:'(358, 82%, 52%)',
          colorcmyk:'(0%, 86%, 82%, 9%)'
        },
        {
          id:3,
          colorid:'four',
          colorname:'Supernova',
          colorhex:'FFD003',
          colorrgb:'(255, 208, 3)',
          colorhsl:'(49, 100%, 51%)',
          colorcmyk:'(0%, 18%, 99%, 0%)'
        },
        {
          id:4,
          colorid:'five',
          colorname:'Thunder',
          colorhex:'211F20',
          colorrgb:'(33, 31, 32)',
          colorhsl:'(330, 3%, 13%)',
          colorcmyk:'(0%, 6%, 3%, 87%)'
        }, 
        {
          id:5,
          colorid:'six',
          colorname:'Scorpion',
          colorhex:'606060',
          colorrgb:'(96, 96, 96)',
          colorhsl:'(0, 0%, 38%)',
          colorcmyk:'(0%, 0%, 0%, 62%)'
        }
        ],
        assets:[],
        filesob:[],
        isSwitched: true,
        isComponentSubActive:false,
        isComponentModalActive:false,
        istabModalActive:false,
        isuploadModalActive:false,
        activetab:0,
        windowWidth: window.innerWidth,
        window:false,
      picker:false,
    logo: false,
    color:false,
    list:true,
    sidebar:false,
    grid:false,
    deletebtn:false,
    isImageModalActive: false,
    activerow: false,
    activeicon:1,
    dropFiles: [],
        colors: {
          hex: '#578CA9',
          rgba: {
            r: 87,
            g: 140,
            b: 169,
            a: 1
          },
          a: 1
        },
         /*Local Storage Varaible*/
        username:localStorage.getItem("username"),
        token:localStorage.getItem("token"),
        /*Local Storage Varaible*/
        }
      },
      methods:{ 
        next(){
          var length = this.files.length
          this.end += 3
          this.start +=3
        },
        previous(){
          var length = this.files.length
          this.end -= 3
          this.start -=3
        },        
        next1(){

          this.end1 += 3
          this.start1 +=3
        },
        previous1(){

          this.end1 -= 3
          this.start1 -=3
        },
        getUserFile(){
          axios({
        method: 'get',
      url:'https://brandsaviouragain.azurewebsites.net/api/User/getUserFile/'+this.username,
      headers: {
        'Content-Type':'application/json',
        'Authorization': 'Bearer ' + this.token,
      }
        }).then(response => {
        console.log(response)
        this.files= response.data
        
        })
            .catch(function (error) {
                console.log(error);
            });
        },
         deleteDropFile(index) {
                this.dropFiles.splice(index, 1)
            },
        deletefile:function(){
          alert(this.fileid)
          axios({
        method: 'get',
      url:'https://brandsaviouragain.azurewebsites.net/api/document/Delete/'+this.fileid,
      headers: {
        'Content-Type':'application/json',
        'Authorization': 'Bearer ' + this.token,
      }
        }).then(response => {
          console.log(response)
          
        })
            .catch(function (error) {
                console.log(error);
            });
        },
        sidebarb:function(){
          this.sidebar ^= true;
          setTimeout(function(){
                this.sidebar = false;
            }, 2000);
        },
      onCopy: function (e) {
        alert('You just copied: ' + e.text)
      },
      onError: function (e) {
        alert('Failed to copy texts')
      },  
      listshow:function(e){
        this.listicon = listicondis ;
      },
        showlist:function(){
          this.list ^=true;
          this.grid = false
        },
        closesec(){
          this.logo = false;
          this.color =false;
        },
      closemodal(){
          this.isComponentModalActive = false;
        },
        showLogo:function(row){
      if(window.innerWidth> 1024)
      {
      
      this.logo = true;
      this.color = false; 
      this.istabModalActive = false;
      this.name = row.fileName;
      this.dimension = row.width +' x '+row.height
      this.width = 0;
      this.height = 0;
      this.src = row.path;
      this.icon = row.iconsrc;
      this.dlsrc = row.path;
      this.contentType = row.contentType;
      this.typeshow = row.contentType.split("/")[1].toUpperCase() ;
      this.type = row.contentType.split("/")[1] ;
          this.icons = 'dist/'+this.type+'.svg';
          this.assetsid=row.index;
          this.activerow = row;
          this.fileid=row.id;
          this.assetsid=row.id;
          this.activerow = row;
      }
      else
      {
      this.logo = true;
      this.color = false; 
      this.istabModalActive = true;
      this.filesarr=row;
      this.name = row.fileName;
      this.dimension = row.width +' x '+row.height
      this.size = row.size;
      this.src = row.path;
      this.icon = row.iconsrc;
      this.dlsrc = row.path;
      this.typeshow = row.contentType.split("/")[1].toUpperCase() ;
      this.type = row.contentType.split("/")[1];
          this.icons = row.iconsrc;
          this.fileid=row.id;
          this.assetsid=row.id;
          this.activerow = row; 
      }
      
        },
    showmodal:function(cl){
      this.isComponentModalActive = true;
      this.name = cl.fileName;
      this.dimension = cl.width +' x '+bl.height
      this.size = cl.size;
      this.src = cl.path;
      this.icon = cl.iconsrc;
      this.dlsrc = cl.path;
      this.type = cl.filetype;
          this.icons = cl.iconsrc;
          this.fileid=cl.id;
          this.assetsid=cl.id;
          this.activerow = cl;
        },

        showColor:function(fl){
          if(window.innerWidth> 1024)
      {
      this.logo = false;
      this.color = true; 
      this.istabModalActive = false;
      this.name = fl.colorname;
      this.dimension = fl.colorhex;
      this.size = fl.colorrgb;
      this.src = fl.colorcmyk;
      this.icon = fl.colorhsl;
      this.type = fl.colorid;
      }
      else
      {
      this.logo = false;
      this.color = true; 
      this.istabModalActive = true;
      this.name = fl.colorname;
      this.dimension = fl.colorhex;
      this.size = fl.colorrgb;
      this.src = fl.colorcmyk;
      this.type = fl.colorid;
      }
      
        },
        downloadfile:function(){
          alert(this.fileid)
          axios({
        method: 'get',
      url:'https://brandsaviouragain.azurewebsites.net/api/Download/DownloadBlob/'+ this.fileid + '/' + this.width + '/' + this.height+ '/'+ this.type + '/'+this.username,

      responseType: "arraybuffer",
      headers: {
        'Content-Type':'application/json',
        'Authorization': 'Bearer ' + this.token,
      }
        }).then(response => {
      
      
      var arrayBuffer = response.data;
      var content = response.headers["content-type"];
      var blob = new Blob([response.data], {type: content});
      FileSaver.saveAs(blob, this.name.split(".")[0] + '.' + this.type);

      // var u8 = new Uint8Array(arrayBuffer);
      //    var b64encoded = btoa([].reduce.call(new Uint8Array(arrayBuffer),function(p,c){return p+String.fromCharCode(c)},''));
      //    console.log(b64encoded);
      //    var mimetype = this.type
      //    this.dataUrl = 'data:' + "image/jpeg" + ';base64,' + b64encoded;
      // var link = document.getElementById('link')
      // console.log(this.name)
      // link.download = this.name.split(".")[0] + ".jpeg"
      // b64encoded = null

      
        }).then(()=> {
        link.click()
      })
            .catch(function (error) {
                console.log(error);
            });
        },
        activeicons:function(al,cl,index){
          this.activeicon = al;
          
          if(this.activeicon == 1){
            this.dlsrc = cl.downloadsrc;
          }
          else if(this.activeicon == 2){
            this.dlsrc = cl.downloadsrc1;
            this.type = 'pdf'
            alert(this.type)
          }         
          else if(this.activeicon == 3){
            this.dlsrc = cl.downloadsrc2;
            this.type = 'jpeg'
            alert(this.type)
          }         
          else if(this.activeicon == 4){
            this.dlsrc = cl.downloadsrc3;
            this.type = 'psd'
            alert(this.type)
          }
          console.log(cl);
          //for assets

          // if(this.activeicon == 1){
          //  this.dlsrc = el[index2].downloadsrc;
          //  console.log(this.dlsrc);
          // }
          // else if(this.activeicon == 2){
          //  this.dlsrc = el[index2].downloadsrc1;
          //  console.log(this.dlsrc);
          // }          
          // else if(this.activeicon == 3){
          //  this.dlsrc = el[index2].downloadsrc2;
          //  console.log(this.dlsrc);
          // }          
          // else if(this.activeicon == 4){
          //  this.dlsrc = el[index2].downloadsrc3;
          //  console.log(this.dlsrc);
          // }
        },
        onChange (val) {
        this.colors = val
      },
      pickershow:function(){
        this.picker^=true
      }

      },

      mounted:function() {
        axios({
        method: 'get',
      url:'https://brandsaviouragain.azurewebsites.net/api/User/getUserFile/'+this.username,
      headers: {
        'Content-Type':'application/json',
        'Authorization': 'Bearer ' + this.token,
      }
        }).then(response => {
        this.files = response.data
        let logoarray=[]
      let assets=[]
        this.files.map(function(value){
        if(value.isLogo === true){
          logoarray.push(value)
          }
          else if(value.isLogo === false){
            assets.push(value)
          }
      })
      this.filesob = logoarray;
      this.assets = assets;
        })
            .catch(function (error) {
                console.log(error)
            })
      
    },
    computed: {
      bgc () {
        return this.colors.hex
      }


    },
  watch: {

      'list':  {
          handler:function (val){
              if(val == true)
              {
                this.grid=false;
              }
              if(val != true){
                this.grid=true;
              }
          }
      },  
      'start':  {
          handler:function (val){
              if(val < 0)
              {
                this.start = 0;
                this.end = 3
              }
              if(val > this.assets.length){
                this.start= this.assets.length
              }
          }
      }, 
      'end':  {
          handler:function (val){
              if(val > this.assets.length)
              {
                this.start = this.assets.length - 3;
                this.end = this.assets.length
              }
              
          }
      },       
       'start1':  {
          handler:function (val){
              if(val < 0)
              {
                this.start1 = 0;
                this.end1 = 3
              }
              if(val > this.filesob.length){
                this.start1= this.filesob.length
              }
          }
      }, 
      'end1':  {
          handler:function (val){
              if(val > this.filesob.length)
              {
                this.start1 = this.filesob.length - 3;
                this.end1 = this.filesob.length
              }
              
          }
      },  
      'activetab':  {
          handler:function (val){
              if(val == 3  )
              {
                this.activetab=0;
              }            
              if(val== -1)
              {
                this.activetab=2;
              }
              
          }
      }, 
      'grid':  {
          handler:function (val){
              if(val == true)
              {
                this.list=false
              }
              if(val != true){
                this.list=true;
              }
          }
      }
  }
  }

