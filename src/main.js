import "babel-polyfill"

import Vue from 'vue'
import page from 'page'
import routes from './routes'
import Buefy from 'buefy'

import style from 'buefy/lib/buefy.css'
import BootstrapVue from 'bootstrap-vue'
import json from './json/data.json'
import VueFire from 'vuefire'
Vue.use(VueFire)

import VueTouch from 'vue-touch'
Vue.use(VueTouch);

import VueClipboard from 'vue-clipboard2'
Vue.use(VueClipboard)

var SocialSharing = require('vue-social-sharing');
Vue.use(SocialSharing);

var VueScrollTo = require('vue-scrollto');
Vue.use(VueScrollTo, {
     container: "body",
     duration: 500,
     easing: "ease-out",
     offset: 0,
     cancelable: true,
     onDone: false,
     onCancel: false
 })

// json.forEach(x => { console.log(x.file[1]); });

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import axios from 'axios'

Vue.use(BootstrapVue);
Vue.use(Buefy,{
  defaultIconPack: 'fa'
})

// console.log(json.id);
const app = new Vue({
  el: '#app',
  data: {
    ViewComponent: { render: h => h('div', 'loading...') }  ,
    sidebar: false
  },
  render (h) { return h(this.ViewComponent) }
})

Object.keys(routes).forEach(route => {
  const Component = require('./pages/' + routes[route] + '.vue')
  page(route, () => app.ViewComponent = Component)
})
page('*', () => app.ViewComponent = require('./pages/404.vue'))
page()


