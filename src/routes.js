export default {
	'/':'index',
	'/new':'new',
	'/addbrand':'addbrand',
	'/signup':'Signup',
	'/login':'Login',
  	'/demo': 'Product',
  	'/product': 'Add',
  	'/help': 'help',
  	'/user':'User',
  	'/upload':'upload',
  	'/setup':'wizard'
}
